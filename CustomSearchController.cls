/**
* @description       : Used to Search within a File in Salesforce
* @author            : Wasef Mohiuddin
**/
public without sharing class CustomSearchController { 
    @AuraEnabled
    public static List<String> searchForIds(String searchText) {
        List<String> contentIds=new List<String>();
        List<String> recordIds=new List<String>();
        for(Record__c searchRecord: [SELECT Id from Record__c]){
            recordIds.add(searchRecord.Id);
        }
        for(ContentDocumentLink docsLink:[SELECT Id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN:recordIds]){
            contentIds.add(docsLink.ContentDocumentId);
        }
        
        List<List<SObject>> results = [FIND :searchText IN ALL FIELDS RETURNING ContentDocument(Id, Title WHERE Id IN:contentIds)];
        List<String> ids = new List<String>();
        for (List<SObject> sobjs : results) {
            for (SObject sobj : sobjs) {
                ids.add(sobj.Id);
            }
        }
        return ids;
    }
    
}

